import React from 'react'
import './keyboard.css'

const alphabet = ['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O',
'P','Q','R','S','T','U','V','W','X','Y','Z']

const KeyBoard = ({usedLetters, onClick, enable}) => <div className="keyBoard">
{alphabet.map((letter, index) =>(
    <button key={index} className="keyBoardTouch" disabled={!enable || usedLetters.includes(letter)} onClick={() => onClick(letter)}>{letter}</button>
))}
</div>

export default KeyBoard