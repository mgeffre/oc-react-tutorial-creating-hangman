import React from 'react'
import KeyBoard from './keyboard'
import hg_1 from './img/hg_1.png'
import hg_2 from './img/hg_2.png'
import hg_3 from './img/hg_3.png'
import hg_4 from './img/hg_4.png'
import hg_5 from './img/hg_5.png'
import hg_6 from './img/hg_6.png'
import hg_7 from './img/hg_7.png'
import './game.css'

const IMG_GAME = [hg_1,hg_2,hg_3,hg_4,hg_5,hg_6,hg_7]

const Game = ({phrase, usedLetters, phraseToShow, numberOfErrors, enableKeyBoard, onLetterClick}) => 
<div className="mainGame">    
    <div className="phraseAndKeyboard">
        <p className="phrase">{phraseToShow}</p>    
        <KeyBoard usedLetters={usedLetters} enable={enableKeyBoard} onClick={onLetterClick} />
    </div>
    <div className="hangmanImg">
        <img src={IMG_GAME[numberOfErrors-1]}/>
    </div>
</div>

export default Game