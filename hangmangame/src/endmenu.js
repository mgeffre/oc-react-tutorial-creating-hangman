import React from 'react'
import loose_img from './img/hg_8.png'
import win_img from './img/win.jpg'

const EndMenu = ({win, onClick}) => 
    <div className="endMenu">
        <div className="imgEndGame">
            <img src={win ? win_img : loose_img}/>
        </div>
        <button className="restartButton" onClick={() => onClick()}>Restart</button>
    </div>

export default EndMenu