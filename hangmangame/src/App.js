import React, { Component } from 'react'
import './App.css';
import EndMenu from './endmenu'
import Game from './game'

const MAX_ERROR = 7

class App extends Component {
  state = {
    phrase : 'CHRONOMETRE',
    usedLetters : [],
    numberOfErrors : 0
  }

  restart= () => {
    var usedLetters = []
    var numberOfErrors = 0
    this.setState({usedLetters:[usedLetters],numberOfErrors:numberOfErrors})
  }

  computeDisplay(phrase, usedLetters){
    return phrase.replace(/\w/g,
      (letter) => (usedLetters.includes(letter) ? letter+' ' : '_ ')
    );
  }

  handleLetterClick = letter => {
    const {phrase, numberOfErrors, usedLetters} = this.state
    var newNnumberOfErrors = numberOfErrors
    if (!phrase.includes(letter)){
      console.log(phrase+' : '+letter)
      newNnumberOfErrors++;
    }
    this.setState({usedLetters:[...usedLetters, letter], numberOfErrors:newNnumberOfErrors})
  }

  checkIfWinOrLoose = () => {
    const {usedLetters, phrase, numberOfErrors} = this.state
    if (!this.computeDisplay(phrase, usedLetters).includes('_') || numberOfErrors===MAX_ERROR){
      return true
    } else {
      return false
    }
  }

  restart(){
    var usedLetters = []
    var numberOfErrors = 0
    this.setState({usedLetters:[usedLetters],numberOfErrors:numberOfErrors})
  }

  render() {
    const {phrase, usedLetters, numberOfErrors} = this.state
    return (
      <div className="App">
        <header>          
        </header>
        <div>
          <Game 
            phrase={phrase} 
            usedLetters={usedLetters} 
            phraseToShow={this.computeDisplay(phrase,usedLetters)} 
            numberOfErrors={numberOfErrors}
            enableKeyBoard={!this.checkIfWinOrLoose()}
            onLetterClick={this.handleLetterClick}
          />
          {this.checkIfWinOrLoose() ? <EndMenu win={numberOfErrors<MAX_ERROR} onClick={this.restart} /> : null}
        </div>
      </div>
    );
  }
}

export default App;
